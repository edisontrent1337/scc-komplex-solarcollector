/**
 * Copyright (c) 2010-2019 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.solarcollector.internal;

import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Energy;
import javax.measure.quantity.Power;
import javax.measure.quantity.Time;

import org.eclipse.smarthome.core.library.types.QuantityType;
import org.eclipse.smarthome.core.library.types.StringType;
import org.eclipse.smarthome.core.library.unit.SmartHomeUnits;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link DaySimulator} is responsible for simulating the environment, light intensity time.
 *
 * @author Team - Initial contribution
 */
public class DaySimulator implements Runnable {

    private int tick = 0;
    private int day = 0;
    private double lightIntensity = 0;
    private int daySeconds = 0;
    private int totalSeconds = 0;

    private double currentWorkOut = 0;
    private double totalWorkOut = 0;

    private double currentPowerOut = 0;

    private SolarCollectorHandler solarCollectionHandler;
    private static final int DAY_TICKS = 1000;
    private static final int SECONDS_OF_DAY = 24 * 60 * 60;
    private static final int TICK_TO_SECOND_FACTOR = SECONDS_OF_DAY / DAY_TICKS;
    private static final Logger LOGGER = LoggerFactory.getLogger(DaySimulator.class);

    private static final float UPDATE_RATE = 1.0f / 10.0f;
    private long lastUpdateMillis = 0;

    private static final float MAX_POWER = 5000;

    public DaySimulator(SolarCollectorHandler solarCollectorHandler) {
        this.solarCollectionHandler = solarCollectorHandler;
        this.tick = 600;
        LOGGER.info("This is the Day Simulator");
    }

    public void update(int increment) {
        tick += increment;
        daySeconds = tick * TICK_TO_SECOND_FACTOR;
        totalSeconds += TICK_TO_SECOND_FACTOR;
        double probability = Math.random();
        float baseChange = 0.02f;
        float changeMagnitude = 1;
        if (probability > 0.9f) {
            changeMagnitude = (float) (Math.random() * 1.5f);
        } else if (probability > 0.95f) {
            changeMagnitude = (float) (Math.random() * 0.5f);
        }

        if (tick > timeToTicks(5, 30) && tick < timeToTicks(7, 0)) {
            if (probability > 0.97f) {
                lightIntensity -= 0.5f * baseChange * changeMagnitude;
            } else {
                lightIntensity += baseChange * changeMagnitude;
                lightIntensity = Math.min(1, lightIntensity);
            }
        }

        else if (tick >= timeToTicks(19, 00)) {
            lightIntensity -= baseChange * 0.3f * changeMagnitude;
            lightIntensity = Math.max(0, lightIntensity);
        }

        if (tick > timeToTicks(4, 0)) {
            lightIntensity += (0.002f + Math.random() * 0.018) * Math.sin(tick / 20 + Math.random() * 5);
        } else {
            lightIntensity = 0;
        }
        lightIntensity = Math.min(1, Math.max(lightIntensity, 0));
        if (tick >= DAY_TICKS) {
            tick = 0;
            day++;
        }
        currentPowerOut = (MAX_POWER) * lightIntensity;
        currentWorkOut = (currentPowerOut * TICK_TO_SECOND_FACTOR) / (3600 * 1000);
        totalWorkOut += currentWorkOut;

        postUpdatesToHandler();
    }

    private void postUpdatesToHandler() {
        solarCollectionHandler.postUpdate(SolarCollectorBindingConstants.WORKLOAD, new QuantityType<Dimensionless>(
                100 * Math.min(1, Math.max(0, lightIntensity)), SmartHomeUnits.PERCENT));
        solarCollectionHandler.postUpdate(SolarCollectorBindingConstants.DAY_TIME,
                new QuantityType<Time>(daySeconds, SmartHomeUnits.SECOND));
        solarCollectionHandler.postUpdate(SolarCollectorBindingConstants.CURRENT_POWER_OUT,
                new QuantityType<Power>(currentPowerOut, SmartHomeUnits.WATT));
        solarCollectionHandler.postUpdate(SolarCollectorBindingConstants.CURRENT_WORK_OUT,
                new QuantityType<Energy>(1000 * currentWorkOut, SmartHomeUnits.WATT_HOUR));

        int seconds = tick * TICK_TO_SECOND_FACTOR;
        int hours = seconds / 3600;
        int minutes = (seconds / 60) - hours * 60;
        solarCollectionHandler.postUpdate(SolarCollectorBindingConstants.FORMATTED_TIME,
                new StringType(String.format("%02d", hours) + ":" + String.format("%02d", minutes)));
    }

    @Override
    public void run() {
        while (true) {
            long now = System.currentTimeMillis();
            if ((now - lastUpdateMillis) / 1000.0f >= UPDATE_RATE) {
                this.update(1);
                lastUpdateMillis = now;
            }
        }
    }

    private int timeToTicks(int hour, int minutes) {
        return (int) (((hour + minutes / 60.0f) / 24.0f) * DAY_TICKS);
    }

}
